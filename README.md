# Kraken Models

A repository to store and document Kraken models created within the TIME US project.

## Instructions

:arrow_right: [jump to our WIKI](https://gitlab.inria.fr/almanach/time-us/kraken-models/-/wikis/home)!


## Model naming convention
- `model_{$name}_{bin|col}_accuracy-{$success-rate}.mlmodel`

`$name` in a name that makes it possible to distinguish models and to identify the data on/for which it was trained.  
`$success-rate` is a number corresponding to the "accuracy report" given by ketos after the training (4 digit number, without "0.").  
`bin|col` makes it possible to precise if the model was trained on binarized or colored images files.  

## Our models:

| model | nb of epochs | description |
| :---- | :----------: | :---------- |
|  model_cphparis58-47-49_bin_accuracy-5253.mlmodel | 8/13 | entraîné sur les GT des dossiers CPH-Paris 1858 et CPH-Paris 1847-49 (Transkribus:ID: 135631 et 135732) soit 221 pages pour 5482 objets d'entraînement. Entraînement avec paramétrage par défaut de Kraken (`ketos train training_bits/*.png`). Attention, le jeu d'entraînement est largement perfectible car le cadrage des morceaux de texte est mauvais.  |
| model_cphparis1847-49_bin_accuracy-4972.mlmodel | 22/27 | entraîné sur les GT du dossier CPH-Paris 1847-49 (Transkribus:ID: 135732) soit 48 pages pour 1539 objets d'entraînement. Entraînement avec paramétrage par défaut de Kraken (`ketos train training_bits/*.png`). Même remarque qu'au-dessus : l'entraînement pourrait sûrement être mieux paramétré. |
| model_cphparis1858_bin_accuracy-5597.mlmodel | 13/18 | entraîné sur les GT du dossier CPH-Paris 1858 (Transkribus:ID: 135631) soit 173 pages pour 3268 objets d'entraînement. Entraînement avec paramétrage par défaut de Kraken sauf pour les partitions (`ketos train training_bits/*.png -p 0.7`). Même remarque qu'au-dessus : l'entraînement pourrait sûrement être mieux paramétré. | 

## More models

You may also take a look at the models created within the [LECTAUREP project](https://gitlab.inria.fr/almanach/lectaurep/models)!